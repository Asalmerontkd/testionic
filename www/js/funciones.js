function alerta(mensaje, titulo) {
	navigator.notification.alert(
    mensaje,  // message
    alertDismissed,         // callback
    titulo,            // title
    'Done'                  // buttonName
	);
}

function alertDismissed() {
    // do something
}

//Inician funciones para el Blink

function onSuccessBlink(response) {
	alerta("OK", "BlinkMethod OK");
	var datos = "data:image/jpg;base64," + response;
	document.getElementById('imgID').src = datos;
	document.getElementById('imgTitulo').innerHTML = "BlinkMethod";
	document.getElementById('imgDesc').innerHTML = "Foto del plugin del blink, onSuccess OK.";
}

function onErrorBlink(response) {
	alerta(response, "BlinkMethod Error");
}

//Inician funciones para INE

function onSuccessDocSm(response) {
	alerta("OK", "Documento INE OK");
	var datos = "data:image/jpg;base64," + response;
	document.getElementById('imgID').src = datos;
	document.getElementById('imgTitulo').innerHTML = "INE Method";
	document.getElementById('imgDesc').innerHTML = "Foto del plugin del INE, onSuccess OK.";
}

function onErrorDocSm(response) {
	alerta(response, "Documento INE Error");
}

//Inician funciones para los Documentos

function onSuccessDocLg(response) {
	alerta("OK", "Documento OK");
	var datos = "data:image/jpg;base64," + response;
	document.getElementById('imgID').src = datos;
	document.getElementById('imgTitulo').innerHTML = "Document Method";
	document.getElementById('imgDesc').innerHTML = "Foto del plugin del documento, onSuccess OK.";
}

function onErrorDocLg(response) {
	alerta(response, "Documento Error");
}

//Inician funciones para las tarjetas

function onSuccessCard(response) {
	alerta("OK "+response.card_type, "Card OK");
	document.getElementById('resultado').innerHTML = response.redacted_card_number;

}

function onErrorCard(response) {
	alerta(response, "Card Error");
}







